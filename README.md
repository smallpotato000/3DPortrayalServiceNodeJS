# 3D Portrayal Service Node (3DPS-N)

The development of 3DPS API on NodeJS

## What is 3DPS?

The 3D Portrayal Service Standard is a geospatial 3D content delivery implementation specification. It focuses on what is to be delivered in which manner to enable interoperable 3D portrayal. [Read more ...](https://www.opengeospatial.org/standards/3dp)

### Prerequisites

NodeJS (That's All :D)

### Installing

A step by step series of examples that tell you how to get a development env running

1. Installing Dependencies

```
npm install
```

2. Run

```
node server.js
```
3. Upload your 3D dataset in the `Assets` folder and update `boundingbox.json` file

4. Running the test

```
http://(host):(port)/service/v1?
service=3DPS&
acceptversions=1.0&
request=GetScene&
boundingbox=6.632137,51.167719,6.758652,51.225485
```
### Example

Request for 3D City Model of Nuess area (3D Tiles format)

http://193.196.37.89:8092/service/v1?service=3DPS&acceptversions=1.0&request=GetScene&boundingbox=6.632137,51.167719,6.758652,51.225485

Request for 3D City Model of Portland area (i3s format)

http://193.196.37.89:8092/service/v1?service=3DPS&acceptversions=1.0&request=GetScene&boundingbox=-122.7019922033274,45.488584596126145,-122.64338178981471,45.54365266746601

## Authors

* **(Joe) T. Santhanavanich <thunyathep.santhanavanich@hft-stuttgart.de>** 
* **Athanasios Koukofikis <athanasios.koukofikis@hft-stuttgart.de>**
* **Prof. Volker Coors <volker.coors@hft-stuttgart.de>** 


