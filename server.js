const express = require("express");
const app = express();
// const request = require("request");
const requestajax = require("ajax-request");
const PORT = process.env.PORT || 8083;
const fs = require('fs');
const boxIntersect = require('box-intersect');

let config_json = fs.readFileSync('config.json');
let config = JSON.parse(config_json);    
let assetFolderName = config['asset_folder_name'];
//const intersects = require('rectangles-intersect');
//const spawn = require("child_process").spawn;
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(express.static(__dirname));

const server = require('http').createServer(app);
server.listen(PORT, function () {
  console.log('The 3DPS Nodejs service running at localhost:%d/', server.address().port);
});



app.get("/service/v1", function (req, res, next) {
  if (typeof req.query.service == "string" && typeof req.query.request == "string" && typeof req.query.acceptversions == "string" && typeof req.query.request == "string" && typeof req.query.boundingbox == "string") {
    // Check Valid Service 
    if (req.query.service == "3DPS" || req.query.service == "3dps") {
      // Check Valid Version
      if (req.query.acceptversions == "1.0") {
        // Check Valid Request
        if (req.query.request == "GetScene") {
          // Check Valid Bounding Box
          var bbox = JSON.parse("[" + req.query.boundingbox + "]");
          if (bbox.length == 4) {
            // Start Checking the boundingboxLookup file
            var contents = fs.readFileSync(assetFolderName + "/boundingboxLookup.json");
            var bbLookup = JSON.parse(contents);
            var count = 0;
            // Looping Through all the boundingboxLookup.json
            console.log('-------start checking the bbox request to the bblookup JSON-------')
            var resultArray = [];
            for (let i = 0; i < bbLookup.length; i++) {
              var boxes = [bbox];
              var bbLookupin = JSON.parse("[" + bbLookup[i].boundingbox + "]");
              boxes.push(bbLookupin);
              console.log(`iteration ${i+1}> Check the intersect of bbox: ${boxes}`);
              // Checking the bbox intersect with boxIntersect library
              // more info: https://www.npmjs.com/package/box-intersect
              var overlap = boxIntersect(boxes);
              if (overlap == '') {
                console.log(`iteration ${i+1}> no overlap`);
              } else {
              console.log(`push overlap result to Array!!`);
              console.log(`push URL:${bbLookup[i].url} `);
                resultArray.push({"url":bbLookup[i].url,"mime": bbLookup[i].mime})
              }
              if (overlap.length > 0) {
                // If the bbox intersect then give the save the Url and Mime type in [resultArray]
                
              }
              count++;
              // Checking for the last iteration done, send the result back to the user
              if (count == bbLookup.length){
                console.log('processing done sending result to client')
                console.log(`result Array : ${resultArray}`)

                if (resultArray.length > 0) {
                  res.send(resultArray);                  
                } else {
                  res.send("No Tileset in the specified Bounding Box");
                }
              }
            }
          } else {
            res.send({ "error": "Invalid Bounding Box" });
          }
        } else if (req.query.request == "GetCapabilities") {
          res.send("... GetCapabilities Under-developing ...");
        } else {
          res.send({ "error": "Only GetScene, GetCapabilities request are available at the moment" });
        }
      } else {
        res.send({ "error": "Only acceptversions 1.0 is available at the moment" });
      }
    } else {
      res.send({ "error": "Service -" + req.query.service + "is not existed" });
    }
  } else {
    res.send({
      "error": "missing parameters => please, check [service] or [request] or [boundingbox] parameters",
    });
  }
});
